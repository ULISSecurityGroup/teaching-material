import requests
import json
import string
import base64


def rot13(s):
    r13 = str.maketrans("ABCDEFGHIJKLMabcdefghijklmNOPQRSTUVWXYZnopqrstuvwxyz","NOPQRSTUVWXYZnopqrstuvwxyzABCDEFGHIJKLMabcdefghijklm")
    return str.translate(s, r13)

def base_64(s):
    return str(base64.b64decode(s))

headers = {
    'authority': 'www.hackthebox.eu',
    'content-length': '0',
    'accept': 'application/json, text/javascript, */*; q=0.01',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.90 Safari/537.36',
    'x-requested-with': 'XMLHttpRequest',
    'origin': 'https://www.hackthebox.eu',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://www.hackthebox.eu/invite',
    'accept-language': 'en-US,en;q=0.9',
}

BASE_URL = "https://www.hackthebox.eu"

response = requests.post(f'{BASE_URL}/api/invite/how/to/generate', headers=headers)

r_text = response.text

j_text = json.loads(r_text)


enc_type = j_text["data"]["enctype"]
data = j_text["data"]["data"]

decoded = ""

if enc_type == "ROT13":
    decoded = rot13(data)
elif enc_type == "BASE64":
    decoded = base_64(data)
else:
    print(f"not implemented {enc_type}")

print(f"decoded text \n{decoded}")

url = decoded.split("to ")[2]

print(url)

response = requests.post(f"{BASE_URL}{url}", headers=headers)

j_code = json.loads(response.text)

print(j_code)

print(base_64(j_code["data"]["code"]))


