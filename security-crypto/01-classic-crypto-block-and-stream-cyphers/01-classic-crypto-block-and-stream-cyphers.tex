\documentclass[aspectratio=169]{beamer}

\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{comment}
\usepackage{amsmath}

\usetikzlibrary{positioning}
\usetheme{ulisse}

\title{Crypto 1 -- Classical Ciphers, Block and Stream ciphers}
\author{Davide Berardi (D)}
\date{29 Feb 2021}

\begin{document}
	\startlayoutpage
	\maketitle
	\stoplayoutpage

	% Intro
	\begin{frame}
		\frametitle{Crypto What?}
		Cryptography is a tool to keep information confidential and
		ensure their integrity.\\

		You use cryptography in every thing you do.  To certify your
		identity (authentication), to make something private
		(authorization), to get billed for just the resource you use
		(accounting).\\

		Nowadays is basically impossible to think about a system without
		cryptography.
	\end{frame}

	\begin{frame}
		\frametitle{Where crypto is useful}
		Cryptography is ubiquious, you find it in every thing you use:
		\begin{itemize}
			\item HTTPS (check it when you surf, please!)
			\item WiFi
			\item E-mails
			\item Encrypted backups (e.g. whatsapp)
			\item Passwords
			\item Cryptocoins
		\end{itemize}

		Security-critical systems SHALL use crypto for everything they implement.
	\end{frame}

	\begin{frame}
		\frametitle{Where crypto is not useful}
		Crypto is not a silver bullet, you cannot think that introducing
		cryptography in your system will make you invulnerable to other kind
		of attacks.

		For instance:
		\begin{itemize}
			\item Safety!
			\item Monitoring, expecially Network monitoring.
			\item Load balancing and network resilience.
			\item IoT and energy harvesting systems.
			\item Anonymity  (but there are tools that relies on cryptography
				to ensure anonymity).
			\item False Security (Double map keys!!!).
		\end{itemize}
	\end{frame}

	% Encoding
	\begin{frame}
		\frametitle{Encoding}
		Data in the machines is not saved differently based on their semantic.
		A string could be interpreted as integers and vice-versa:

		The users apply meaning to it, the data (normally) do not carry any
		information on how to interpret them.

		\begin{itemize}
			\item hex values: 0x41 0x42 0x43 0x44
			\item ASCII:      A B C D
			\item Integer Little endian: 1145258561
		\end{itemize}

		This is also relevant for software security.
	\end{frame}

	\begin{frame}
		\frametitle{Encoding, ASCII}
		The ubiquitous encoding you encounter every time is ASCII

		\begin{center}
		\includegraphics[width=\textwidth]{manascii}
		\end{center}

		Bytes are therefore translated and interpreted as characters.
		No special algorithm is required to inteprete characters.
	\end{frame}

	\begin{frame}
		\frametitle{Encoding}
		But, where these different representations start to be useful?
		\\

		In the field of data representation, one of the key concept is the idea of
		``bad char'' and ``escape''.

		Let's suppose that we need to send e-mail using SMTP.

		\begin{quote}
		SMTP commands and, unless altered by a service extension,
			message data, are transmitted in "lines". Lines consist
			of zero or more data characters terminated by the
			sequence ASCII character "CR" (hex value 0D) followed
			immediately by ASCII character "LF" (hex value 0A).
		\end{quote}

		but...if we need to send an image as an attachment?  What if this image
		contains 0x0d 0x0a?
	\end{frame}

	\begin{frame}
		\frametitle{Encoding, base64, hexencoding}
		\begin{minipage}{.49\textwidth}
			\includegraphics[width=1.1\textwidth]{base64}
		\end{minipage}
		\begin{minipage}{.49\textwidth}
			\includegraphics[width=\textwidth]{hexencoding}
		\end{minipage}
	\end{frame}

	% Classical cipher
	\begin{frame}
		\frametitle{Crypto}

		Encoding only relies on the algorithm and do not poses
		any security in place against cryptoanalysis (more on this later).

		Crypto is the set of procedures and algorithms that can be used to
		ensure two proprieties:

		\begin{center}
		\begin{tikzpicture}
			\node[minimum width=1cm, minimum height=1cm] (Alice) {\includegraphics[scale=.33]{osa_user_blue}};
			\node[below=0em of Alice] {Alice};
			\node[minimum width=1cm, minimum height=1cm, right=1cm of Alice] (Middle) {};
			\node[minimum width=1cm, minimum height=1cm, right=1cm of Middle] (Bob) {\includegraphics[scale=.33]{osa_user_green}};
			\node[below=0em of Bob] {Bob};
			\node[minimum width=1cm, minimum height=1cm, below=.5cm of Middle] (Eve) {\includegraphics[scale=.33]{osa_user_black_hat}};
			\node[below=0em of Eve] {Eve};

			\draw[-latex] (Alice) -- (Bob);
		\end{tikzpicture}
		\end{center}

		\begin{itemize}
			\item Confidentiality: The information sent from Alice to Bob
				are only readable by the authorized parts.
			\item Integrity: The data that are transmitted from Alice to Bob
				are not altered.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Crypto key points}
		We will focus on ciphers, the ``algorithms'' and the way to use them
		to ensure these two proprieties.  There are two great families of
		ciphers:

		\begin{itemize}
			\item Symmetric ciphers
			\item Asymmetric ciphers
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Crypto key points}
		They share the same basic concepts, they have two functions:
		\begin{itemize}
			\item $C(k_1,m) \to c$, where $k_1$ is called a key,
				$m$ is the payload (the data to send), and $c$
				is the encrypted data (the one that will actually
				get sent to the Destination).
			\item $D(k_2,c) \to m$, which is the respective
				decryption function for $C$.
		\end{itemize}

		In symmetric ciphers, $k_1 = k_2$, everyone that possess the key
		can encrypt and decrypt plaintexts and ciphertexts.\\
		\vspace{1em}

		With asymmetric ciphers, the two keys differs, oversimplifying:
		there is a key to decrypt data ($k_2$) and a key to encrypt data
		($k_1$).\\
		\vspace{1em}

		Also, using Asymmetric ciphers, you can define more functions
		to sign and verify data (and therefore ensure integrity).
	\end{frame}

	\begin{frame}
		\frametitle{Classical ciphers: Transposition}
		The basic ciphers that can be explored are of two families:
		Transposition, in which a byte of the payload get
				arranged in a different position.\\
			\vspace{1em}
			\begin{center}
			\begin{tabular}{c|c|c|c|c}
				C & I & A & O & Q \\
				U & E & S & T & O \\
				E & U & N & M & E \\
				S & S & A & G & G \\
				I & O & C & I & F \\
				R & A & T & O & . \\
			\end{tabular}
			\end{center}
			Reading the columns we will get:
			CUESIRIEUSOAASNACTOTMGIOQOEGF.
	\end{frame}

	\begin{frame}
		\frametitle{Classical ciphers: Subsitution}
		The basic ciphers that can be explored are of two families:
		Substituion, in which the bytes of the payload gets substituted
		by a different byte.

		\begin{center}
		\begin{tikzpicture}
			\node [draw, minimum width=1em, minimum height=2em] (A) {\texttt{A}};
			\node [right=0em of A,draw, minimum width=1em, minimum height=2em] (B) {\texttt{B}};
			\node [right=0em of B,draw, minimum width=1em, minimum height=2em] (C) {\texttt{C}};
			\node [right=0em of C,draw, minimum width=1em, minimum height=2em] (D) {\texttt{D}};
			\node [right=0em of D,draw, minimum width=1em, minimum height=2em] (E) {\texttt{E}};
			\node [right=0em of E,draw, minimum width=1em, minimum height=2em] (F) {\texttt{F}};
			\node [right=0em of F,draw, minimum width=1em, minimum height=2em] (G) {\texttt{G}};
			\node [right=0em of G,draw, minimum width=1em, minimum height=2em] (H) {\texttt{H}};
			\node [right=0em of H,draw, minimum width=1em, minimum height=2em] (I) {\texttt{I}};
			\node [right=0em of I,draw, minimum width=1em, minimum height=2em] (J) {\texttt{J}};
			\node [right=0em of J,draw, minimum width=1em, minimum height=2em] (K) {\texttt{K}};
			\node [right=0em of K,draw, minimum width=1em, minimum height=2em] (L) {\texttt{L}};
			\node [right=0em of L,draw, minimum width=1em, minimum height=2em] (M) {\texttt{M}};
			\node [right=0em of M,draw, minimum width=1em, minimum height=2em] (N) {\texttt{N}};
			\node [right=0em of N,draw, minimum width=1em, minimum height=2em] (O) {\texttt{O}};
			\node [right=0em of O,draw, minimum width=1em, minimum height=2em] (P) {\texttt{P}};
			\node [right=0em of P,draw, minimum width=1em, minimum height=2em] (Q) {\texttt{Q}};
			\node [right=0em of Q,draw, minimum width=1em, minimum height=2em] (R) {\texttt{R}};
			\node [right=0em of R,draw, minimum width=1em, minimum height=2em] (S) {\texttt{S}};
			\node [right=0em of S,draw, minimum width=1em, minimum height=2em] (T) {\texttt{T}};
			\node [right=0em of T,draw, minimum width=1em, minimum height=2em] (U) {\texttt{U}};
			\node [right=0em of U,draw, minimum width=1em, minimum height=2em] (V) {\texttt{V}};
			\node [right=0em of V,draw, minimum width=1em, minimum height=2em] (W) {\texttt{W}};
			\node [right=0em of W,draw, minimum width=1em, minimum height=2em] (X) {\texttt{X}};
			\node [right=0em of X,draw, minimum width=1em, minimum height=2em] (Y) {\texttt{Y}};
			\node [right=0em of Y,draw, minimum width=1em, minimum height=2em] (Z) {\texttt{Z}};
			\node [below=1em of A, draw, minimum width=1em, minimum height=2em] (A2) {\texttt{N}};
			\node [right=0em of A2,draw, minimum width=1em, minimum height=2em] (B2) {\texttt{O}};
			\node [right=0em of B2,draw, minimum width=1em, minimum height=2em] (C2) {\texttt{P}};
			\node [right=0em of C2,draw, minimum width=1em, minimum height=2em] (D2) {\texttt{Q}};
			\node [right=0em of D2,draw, minimum width=1em, minimum height=2em] (E2) {\texttt{R}};
			\node [right=0em of E2,draw, minimum width=1em, minimum height=2em] (F2) {\texttt{S}};
			\node [right=0em of F2,draw, minimum width=1em, minimum height=2em] (G2) {\texttt{T}};
			\node [right=0em of G2,draw, minimum width=1em, minimum height=2em] (H2) {\texttt{U}};
			\node [right=0em of H2,draw, minimum width=1em, minimum height=2em] (I2) {\texttt{V}};
			\node [right=0em of I2,draw, minimum width=1em, minimum height=2em] (J2) {\texttt{W}};
			\node [right=0em of J2,draw, minimum width=1em, minimum height=2em] (K2) {\texttt{X}};
			\node [right=0em of K2,draw, minimum width=1em, minimum height=2em] (L2) {\texttt{Y}};
			\node [right=0em of L2,draw, minimum width=1em, minimum height=2em] (M2) {\texttt{Z}};
			\node [right=0em of M2,draw, minimum width=1em, minimum height=2em] (N2) {\texttt{A}};
			\node [right=0em of N2,draw, minimum width=1em, minimum height=2em] (O2) {\texttt{B}};
			\node [right=0em of O2,draw, minimum width=1em, minimum height=2em] (P2) {\texttt{C}};
			\node [right=0em of P2,draw, minimum width=1em, minimum height=2em] (Q2) {\texttt{D}};
			\node [right=0em of Q2,draw, minimum width=1em, minimum height=2em] (R2) {\texttt{E}};
			\node [right=0em of R2,draw, minimum width=1em, minimum height=2em] (S2) {\texttt{F}};
			\node [right=0em of S2,draw, minimum width=1em, minimum height=2em] (T2) {\texttt{G}};
			\node [right=0em of T2,draw, minimum width=1em, minimum height=2em] (U2) {\texttt{H}};
			\node [right=0em of U2,draw, minimum width=1em, minimum height=2em] (V2) {\texttt{I}};
			\node [right=0em of V2,draw, minimum width=1em, minimum height=2em] (W2) {\texttt{J}};
			\node [right=0em of W2,draw, minimum width=1em, minimum height=2em] (X2) {\texttt{K}};
			\node [right=0em of X2,draw, minimum width=1em, minimum height=2em] (Y2) {\texttt{L}};
			\node [right=0em of Y2,draw, minimum width=1em, minimum height=2em] (Z2) {\texttt{M}};

			\draw (A) -- (A2);
			\draw (B) -- (B2);
			\draw (C) -- (C2);
			\draw (D) -- (D2);
			\draw (E) -- (E2);
			\draw (F) -- (F2);
			\draw (G) -- (G2);
			\draw (H) -- (H2);
			\draw (I) -- (I2);
			\draw (J) -- (J2);
			\draw (K) -- (K2);
			\draw (L) -- (L2);
			\draw (M) -- (M2);
			\draw (N) -- (N2);
			\draw (O) -- (O2);
			\draw (P) -- (P2);
			\draw (Q) -- (Q2);
			\draw (R) -- (R2);
			\draw (S) -- (S2);
			\draw (T) -- (T2);
			\draw (U) -- (U2);
			\draw (V) -- (V2);
			\draw (W) -- (W2);
			\draw (X) -- (X2);
			\draw (Y) -- (Y2);
			\draw (Z) -- (Z2);
			\draw (X) -- (X2);
		\end{tikzpicture}

		\end{center}

		For instance, Caesar cipher (ROT13) is a substitution cipher.
	\end{frame}

	\begin{frame}
		\frametitle{Cipher attacks: classification}
		In cryptography attacks are generally classified using this schema:

		\begin{itemize}
			\item Only ciphertext known: You know only the ciphertext.
			\item Known plaintext attack: You know the plaintext encrypted
				and you want to get the key.
			\item Chosen plaintext attack: You can cypher everything you want.
			\item Chosen ciphertext attack: You can decrypt everything you want.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Cracking methods: bruteforce}
		Bruteforce is always a possible solution.  It is simply not feasible enough.
		Let's formalize Caesar's cipher:
		\begin{itemize}
			\item $k \in {0..26}$
			\item $C(k, m) \to c$ and $D(k, c) \to m$ are just ${ (m_i + k)~mod~26 }$ for each $m_i$ in $m$ or $c$.
		\end{itemize}

		The key-space is tiny (26 elements), we can just brute force the key and analyze the output.\\
		\vspace{1em}
		For real world keys, we have numbers of security bits of the
		key such as 256 bits, therefore you have a bruteforce space of:
		$2^{256} =
		115792089237316195423570985008687907853269984665640564039457584007913129639936$,
		a for cycle over this kind of elements using modern computers will take basically
		forever.\\
		\vspace{1em}

		Note that this sentence must take in consideration the evolution of the computing powers.
	\end{frame}

	\begin{frame}
		\frametitle{Cracking methods: statistical analysis}
		If your cipher only randomize the placement of the letters, you can
		retrieve information of the plaintext.\\
		\vspace{1em}

		\begin{center}
		\includegraphics[scale=.5]{englishfreq}\\
			{\tiny source: \url{https://www3.nd.edu/~busiforc/handouts/cryptography/letterfrequencies.html}}
		\end{center}

	\end{frame}

	\begin{frame}
		\frametitle{Cracking methods: statistical analysis}
		The base idea is that natural language, maintains a specific
		frequency of symbols.\\
		\vspace{1em}

		For instance in English, ``e'' is the most common letter, followed by
		``t'' and so on.  Also, if your cipher does not randomize
		letter order and does not remove split points (spaces,
		newlines), you can analyze bi-grams (``of'', ``at'', ...),
		tri-grams (``and'', ``for'', ...).
	\end{frame}

	\begin{frame}
		\frametitle{Security parameters of a cipher: diffusion and confusion}
		Ciphers are therefore analyzed using two proprieties:
		\begin{itemize}
			\item Confusion: the (un-)correlation between the key and the ciphertext.
				You can't guess something of the key from the ciphertext.
			\item Diffusion: the (un-)correlation between the plaintext and the ciphertext.
				You can't guess something of the payload (e.g.
				the frequency of the letter) from the
				ciphertext.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Security by obscurity: RC4}
		\begin{center}
			\Huge Do not rely on the security using only your algorithm!
		\end{center}

		At a first glance, you may think that the algorithm must be secret.
		\begin{itemize}
			\item That's totally wrong.
			\item Keeping your algorithm secret is only a little deterrent
				to attack your system.
			\item There are some horror stories, such as RC4.
		\end{itemize}
	\end{frame}

	% Perfect Cipher XOR
	\begin{frame}
		\frametitle{The perfect cipher: One time pad}
		Shannon defines the security of a cipher as the impossibility
		of guessing the bits of the plaintext from the ciphertext
		without knownledge of the key.\\
		\vspace{1em}

		probability of guessing bit of plaintext from ciphertext:
		\begin{center}
		\begin{tikzpicture}
			\node[draw, minimum height=2em, minimum width=2em] (b1) {50\%};
			\node[right=0em of b1, draw, minimum height=2em, minimum width=2em] (b2) {50\%};
			\node[right=0em of b2, draw, minimum height=2em, minimum width=2em] (b3) {50\%};
			\node[right=0em of b3, draw, minimum height=2em, minimum width=2em] (b4) {50\%};
			\node[right=0em of b4, draw, minimum height=2em, minimum width=2em] (b5) {50\%};
			\node[right=0em of b5, draw, minimum height=2em, minimum width=2em] (b6) {50\%};
			\node[right=0em of b6, draw, minimum height=2em, minimum width=2em] (b7) {50\%};
			\node[right=0em of b7, draw, minimum height=2em, minimum width=2em] (b8) {50\%};
		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{The perfect cipher: One time pad}
		The way to implement a perfect cipher is the simplest you can imagine: exclusive or.

		XOR is an operation which gives one when the two bits are different:
		\begin{center}
		\begin{tabular}{r|r||l}
			A & B & Out\\
			\hline\\
			0 & 0 & 0 \\
			0 & 1 & 1 \\
			1 & 0 & 1 \\
			1 & 1 & 0
		\end{tabular}
		\end{center}

		We can construct a cypher using a XOR simply xoring every bit of the key with every
		bit of the payload.

		\begin{itemize}
			\item $C(k,m) = k \oplus m = c$
		\end{itemize}

		XOR is obviously invertable, reapplying xor using one of the two values (m or k) you
		will get the other one:
		\begin{itemize}
			\item $D(k,c) = k \oplus c = m$
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Why perfect?  A demo}

		\begin{itemize}
			\item $C("abcdefghijklmn","cyberchallenge")=0x02~0x1b~0x01...=c$
			\item $D("abcdefghijklmn",c) = "cyberchallenge"$
			\item $D("qw...", c) = "slap like now!"$
		\end{itemize}

		You can generate every payload from the cyphertext.
	\end{frame}

	\begin{frame}
		\frametitle{Which are the downsides?  Why no one uses the One time pad cipher?}
		This kind of cipher is virtually inusable.  You need to generate random
		keys with the same size of the payload you need to encrypt.\\
		\vspace{1em}

		Think about NetFlix with gigabytes of data that gets sent...
		They need to send to you a bunch of hard drives full of random bytes? :)\\
		\vspace{1em}

		Also, there are a key concept in cryptography: who ensure that the
		key sent from netflix is the real one?  Your neighborn hasn't stole it?\\
		\vspace{1em}
		\begin{center}
		\begin{tikzpicture}
			\node[minimum width=1cm, minimum height=1cm] (Alice) {\includegraphics[scale=.33]{osa_user_blue}};
			\node[below=0em of Alice] {Alice};
			\node[minimum width=1cm, minimum height=1cm, right=1cm of Alice] (Middle) {};
			\node[minimum width=1cm, minimum height=1cm, right=1cm of Alice] (Middle) {\includegraphics[scale=.33]{osa_user_black_hat}};
			\node[minimum width=1cm, minimum height=1cm, right=1cm of Middle] (Bob) {\includegraphics[scale=.33]{osa_user_green}};
			\node[below=0em of Bob] {Bob};

			\draw[-latex] (Alice) -- (Bob);
		\end{tikzpicture}
		\end{center}

		If you repeat the key, you get another cypher: Many time pad, which is
		totally insecure.
	\end{frame}

	\begin{frame}
		\frametitle{Many time pad}
		Totally random keys and infinite streams are unusable.  You need to come up with
		a solution that can be feasible.\\
		\vspace{1em}

		The first thing that were explored by Vigenere was to re-use the key.
		If you re-use the key you get vulnerable to frequency attacks.\\
		\vspace{1em}

		First you calculate index of coincidence. IoC is the recurring characters in
		a sentence.
		\begin{equation}
			IoC(s) = 26 \sum\limits_{i=A}^Z count(i,s) (count(i,s) - 1) / len(s) (len(s) - 1)
		\end{equation}

		The key length is a multiple of IoC, therefore you can guess the key length.

		After that you can still use frequency attacks.
	\end{frame}

	% Block Ciphers
	\begin{frame}[fragile]
		\frametitle{What a block cipher is?}
		XOR cipher can be used with a granularity of single bits.
		Modern ciphers use a different concept, a granularity of
		a ``block''.  That is, if you encrypt a single bit, you
		will get a full block in output.

		\begin{center}
		\begin{tikzpicture}
			\node[minimum width=2cm, minimum height=2cm] (Input) {Input};
			\node[right=1em of Input,minimum width=2cm, minimum height=2cm, draw] (C) {AES};
			\node[right=1em of C,minimum width=2cm, minimum height=2cm] (Output) {Output Block size};

			\draw[-latex] (Input) -- (C);
			\draw[-latex] (C) -- (Output);
		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{AES}
		AES is one of the most common symmetric ciphers.

		It works by using a so called substitution-permutation network.

		Very briefly, it uses substitutions and permutations to obtain confusion and
		diffusion.
		It is belief to be secure against most of the attacks, it has the following parameters:
		\begin{itemize}
			\item Key Size: 128b, 192b, 256b (16-32B)
			\item Block Size: 128b (16B)
		\end{itemize}

		Normally, you use an accelerated version in hardware, there are
		more feasible ciphers if you are tied to software (CPU).
	\end{frame}

	\begin{frame}
		\frametitle{Multiple encryption}
		You should combine encryption carefully, for instance if you
		do two rounds of AES, you will not multiply the security factor
		(in term of bits of the key) by two.\\
		\vspace{1em}

		You need at least three round of encryption (3DES, 3AES, ...)
	\end{frame}

	\begin{frame}
		\frametitle{Multiple encryption: 2AES}
		There is an attack called ``Meet in the middle''\footnote{Do not
		associate it to Man in the middle, it is a totally unrelated thing :)}.

		This attack is a \emph{known plaintext}.  You know the ciphertext and the
		plaintext and you want to obtain the key.

		You start by encrypting the plaintext bruteforcing the key.  In the meantime
		you decrypt the ciphertext with another key and you save the results.

		When you find something that verifies: $C(k,p) == D(k,c)$ it means that
		you've found the key.\\

		Basically you've added a single bit of security to your key.
	\end{frame}

	% Stream Ciphers
	\begin{frame}
		\frametitle{What a stream cipher is?}
		The problem with Block cyphers is that you can't encrypt a payload
		bigger than one block length.\\
		\vspace{1em}

		Given that the blocksize is tipically just a low number of
		bytes (e.g. 16), the cipher would be really unuseful.\\
		\vspace{1em}

		The normal thing you do on the internet (think about streaming
		videos) is to have a \emph{stream} of data.  Therefore you
		would need a continuous generation of ciphertext, starting from a key.\\
		\vspace{1em}

		\begin{center}
		\begin{tikzpicture}
			\node[draw] (C) {C};
			\node[below=1em of C] (middle) {$\oplus$};
			\node[left=3em of middle] (input) {Input};
			\node[right=3em of middle] (output) {Output};

			\draw[-latex] (C) -- (middle);
			\draw[-latex] (input) -- (middle);
			\draw[-latex] (middle) -- (output);
		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{From block to stream: operational modes}
		A block cipher can be transformed to somewhat similar to a stream
		one.  That's possible using some techniques called \emph{Operational
		modes}.

		Basically we see the following modes:
		\begin{itemize}
			\item ECB (Electronic code book), DANGEROUS but really easy to implement;
			\item CBC (Cipher block chain), one of the most common modes;
			\item CTR (Counter), used in WiFi (WPA2) and really easy to implement.
		\end{itemize}

		There are a lot of other modes:
		\begin{itemize}
			\item XTS (Cipher text stealing), used in disk
				encryption because you don't need to pad the last block.
			\item GCM (Galois counter mode), which is one of the moderns.
		\end{itemize}

		We don't have time to see them, but they're very similar to the one presented.
	\end{frame}

	\begin{frame}
		\frametitle{ECB}
		ECB is the simplest of the methods.

		You encrypt every block with the same key.

		\begin{center}
		\begin{tikzpicture}
			\node[minimum width=8em] (p1) {plain 1};
			\node[draw, minimum width=8em, below=2em of p1] (enc1) {AES};
			\node[minimum width=8em, below=2em of enc1] (c1) {cipher 1};
			\node[left=0em of enc1] (k1) {k};

			\node[right=1cm of p1, minimum width=8em] (p2) {plain 2};
			\node[draw, minimum width=8em, below=2em of p2] (enc2) {AES};
			\node[minimum width=8em, below=2em of enc2] (c2) {cipher 2};
			\node[left=0em of enc2] (k2) {k};

			\node[right=1cm of p2, minimum width=8em] (p3) {plain 3};
			\node[draw, minimum width=8em, below=2em of p3] (enc3) {AES};
			\node[minimum width=8em, below=2em of enc3] (c3) {cipher 3};
			\node[left=0em of enc3] (k3) {k};

			\draw[-latex] (p1) -- (enc1);
			\draw[-latex] (enc1) -- (c1);

			\draw[-latex] (p2) -- (enc2);
			\draw[-latex] (enc2) -- (c2);

			\draw[-latex] (p3) -- (enc3);
			\draw[-latex] (enc3) -- (c3);
		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{ECB}
		The downside is that you can still get information from the repetition
		of 16 bytes:
		\begin{itemize}
			\item $m_1 = "AAAAAAAAAAAAAAAA" \to c_1 = "1234567890123456"$
			\item $m_2 = "AAAAAAAAAAAAAAAB" \to c_2 = "XXXXXXXXXXXXXXXX"$
			\item $m_3 = "AAAAAAAAAAAAAAAA" \to c_3 = "1234567890123456"$
		\end{itemize}
		That is really problematic for big inputs that can have repeated values.

		\begin{center}
			\includegraphics[scale=.5]{Tux_ecb}\\
			{\tiny source: filippo.io}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{CBC}
		In CBC you XOR the input with the previous block.  Therefore you
		will make every block \emph{dependent} from the previous one.

		\begin{center}
		\begin{tikzpicture}
			\node[minimum width=8em] (p1) {plain 1};
			\node[above=.5cm of p1, minimum width=2em] (xor1) {$\oplus$};
			\node[left=.5cm of xor1, minimum width=2em] (iv1) {$IV$};
			\node[draw,above=.5cm of xor1, minimum width=2em] (enc1) {AES};
			\node[right=3em of enc1] (middle1a) {};
			\node[below=.75cm of middle1a] (middle1b) {};
			\node[left=0em of enc1] (k1) {k};

			\node[right=1cm of p1, minimum width=8em] (p2) {plain 2};
			\node[above=.5cm of p2, minimum width=2em] (xor2) {$\oplus$};
			\node[draw,above=.5cm of xor2, minimum width=2em] (enc2) {AES};
			\node[right=3em of enc2] (middle2a) {};
			\node[below=.75cm of middle2a] (middle2b) {};
			\node[left=0em of enc2] (k2) {k};

			\node[right=1cm of p2, minimum width=8em] (p3) {plain 3};
			\node[above=.5cm of p3, minimum width=2em] (xor3) {$\oplus$};
			\node[draw,above=.5cm of xor3, minimum width=2em] (enc3) {AES};
			\node[right=3em of enc3] (middle3a) {};
			\node[below=.75cm of middle3a] (middle3b) {Output};
			\node[left=0em of enc3] (k3) {k};

			\draw[-latex] (p1) -- (xor1);
			\draw[-latex] (iv1) -- (xor1);
			\draw[-latex] (xor1) -- (enc1);
			\draw (enc1) -- (middle1a.center);
			\draw (middle1a.center) -- (middle1b.center);
			\draw[-latex] (middle1b.center) -- (xor2);

			\draw[-latex] (p2) -- (xor2);
			\draw[-latex] (xor2) -- (enc2);
			\draw (enc2) -- (middle2a.center);
			\draw (middle2a.center) -- (middle2b.center);
			\draw[-latex] (middle2b.center) -- (xor3);

			\draw[-latex] (p3) -- (xor3);
			\draw[-latex] (xor3) -- (enc3);
			\draw (enc3) -- (middle3a.center);
			\draw[-latex] (middle3a.center) -- (middle3b);

		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{CBC}
		This approach can be problematic, suppose to want to
		parallelize the computation or you lose one of the blocks in
		the middle.\\
		\vspace{1em}

		The first block gets XORed with a random value (which gets sent with the
		ciphertext).  That value is called the Initialization Vector (IV).\\
		\vspace{1em}

		You \textbf{must not} reuse an initialization vector.
	\end{frame}

	\begin{frame}
		\frametitle{CTR}
		CTR is a mode mainly used in embedded systems.

		In this mode you simply encrypt an IV with the key and then XOR it
		to the payload.  You need to change the IV for every block (more on
		this later).

		\begin{center}
		\begin{tikzpicture}
			\node[] (IV1) {$IV_{a}$};
			\node[right=.3cm of IV1,draw] (enc1) {AES};
			\node[right=.3cm of enc1] (xor1) {$\oplus$};
			\node[above=0em of enc1] (k1) {k};
			\node[above=1em of xor1] (p1) {p1};
			\node[below=1em of xor1] (o1) {c1};

			\node[right=1cm of xor1] (IV2) {$IV_{b}$};
			\node[right=.3cm of IV2,draw] (enc2) {AES};
			\node[right=.3cm of enc2] (xor2) {$\oplus$};
			\node[above=0em of enc2] (k2) {k};
			\node[above=1em of xor2] (p2) {p2};
			\node[below=1em of xor2] (o2) {o2};

			\node[right=1cm of xor2] (IV3) {$IV_{c}$};
			\node[right=.3cm of IV3,draw] (enc3) {AES};
			\node[right=.3cm of enc3] (xor3) {$\oplus$};
			\node[above=0em of enc3] (k3) {k};
			\node[above=1em of xor3] (p3) {p3};
			\node[below=1em of xor3] (o3) {o3};

			\draw[-latex] (IV1) -- (enc1);
			\draw[-latex] (enc1) -- (xor1);
			\draw[-latex] (p1) -- (xor1);
			\draw[-latex] (xor1) -- (o1);

			\draw[-latex] (IV2) -- (enc2);
			\draw[-latex] (enc2) -- (xor2);
			\draw[-latex] (p2) -- (xor2);
			\draw[-latex] (xor2) -- (o2);

			\draw[-latex] (IV3) -- (enc3);
			\draw[-latex] (enc3) -- (xor3);
			\draw[-latex] (p3) -- (xor3);
			\draw[-latex] (xor3) -- (o3);
		\end{tikzpicture}
		\end{center}
	\end{frame}

	\begin{frame}
		\frametitle{CTR}
		After that you can apply a function (ideally increment by one)
		to the IV and continue like that.\\
		\vspace{1em}

		It is totally parallelizable against different cores and is not
		subsceptible to data loss (you just need to know the IV value
		and the number of the packet).\\
		\vspace{1em}

		Also, it does not use decryption (you simply XOR encrypted
		value to the payload), which is nice, because AES decryption is
		much slower than encryption.
	\end{frame}

	\startlayoutpage
	\begin{frame}
		\begin{center}
			\Huge Questions?
		\end{center}
	\end{frame}
	\stoplayoutpage
\end{document}
