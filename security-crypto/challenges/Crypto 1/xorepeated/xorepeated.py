#!/usr/bin/env python3

# author: @meowmeowxw

from secret import flag, key
from math import ceil


def xor(s1, s2):
    return bytes(a ^ b for a, b in zip(s1, s2))


# key = ciao
# plaintext = bellabellabella
# ciphertext = bellabellabella xor
#              ciaociaociaociao
# flag: ulissecc21{XXXXXXXXXXXXXXXXXXXXX} xor
# xor:  ABCDEFGHIJK...ABCDEFGHIJK...ABCDEFGHIJK
#
enc = xor(flag, key * ceil(len(flag) / len(key)))
print(enc.hex())
# enc = 010458002c0c103c5954020d58442c385907000656261a01521600031c3d4a442c2b5c434000082c3358560d2b000510340c013358564e185b024422
