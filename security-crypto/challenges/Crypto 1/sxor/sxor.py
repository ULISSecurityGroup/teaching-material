#!/usr/bin/env python3

from secret import flag, k

enc = []
for f in flag:
    enc.append(f ^ k)

print(enc)
# [199, 222, 219, 193, 193, 215, 209, 209, 128, 131, 201, 218, 215, 222, 222, 130, 237, 203, 130, 199, 237, 202, 130, 192, 215, 214, 237, 223, 215, 147, 215, 218, 215, 218, 215, 218, 129, 218, 207]
print(bytes(enc).hex())
# c7dedbc1c1d7d1d18083c9dad7dede82edcb82c7edca82c0d7d6eddfd793d7dad7dad7da81dacf
