La challenge genera due keys:

key1 = '0' * 13 + 'xyz'
key2 = 'abc' + '0' * 13

dove a,b,c e x,y,z sono byte random tra 30 e 80

dopodiche cifra il plaintext 'a' * 16 in questo modo (Known Plaintext)

ciphertext = AES.encrypt(key2, AES.encrypt(key1, plaintext))

Infine allo stesso modo cifra la flag (Not Known):

AES.ecnrypt(key2, AES.encrypt(key1, flag))

> Come attaccare?

in questo caso se avesse cifrato una volta sola con una chiave di questo tipo:

'abc' + '0' * 10 + 'xyz'

dove 'abcxyz' sono random sarebbe stato più sicuro. Perché?

> avremmo dovuto bruteforcare 50^6 chiavi diverse per trovare il plaintext (15625000000)

> Meet in the middle

In questo caso possiamo montare un known plaintext attack per creare un meet in the middle
algorithm per diminuire il calcolo da fare nel bruteforce.

Step 1: creiamo una hash table/dizionario precomputato di tutte le possibili encryption di 'a' * 16 con la chiave key1. ex:
        table[AES.encrypt('0000000000000123', 'aaaaaaaaaaaaaaaa')] = 0000000000000123
        table[AES.encrypt('0000000000000124', 'aaaaaaaaaaaaaaaa')] = 0000000000000124
        table[AES.encrypt('0000000000000125', 'aaaaaaaaaaaaaaaa')] = 0000000000000125
        ....
        
        Costo computazionale -> 50^3 = 125000

Step 2: proviamo a decifrare a partire dal ciphertext di 'a' * 16 con tutte le possibili chiavi key2. ex:
        pt1 = AES.decrypt('1230000000000000', ciphertext)
        pt2 = AES.decrypt('1240000000000000', ciphertext)
        pt3 = AES.decrypt('1250000000000000', ciphertext)

        E controlliamo se questi plaintext sono presenti nella tabella creata precedentemente.

        if pt1 in table:
            key1 = table[pt1]

        Se si abbiamo effettuato un meet in the middle e abbiamo trovato le due chiavi key1, key2.

        Costo computazionale -> ~50^3 = 125000

Costo computazionale totale -> 50^3 + 50^3 = 2*(50^3) = 250000 << 15625000000


> Esempi di meet in the middle
2DES: https://secgroup.dais.unive.it/teaching/cryptography/meet-in-the-middle/
Baby step giant step to solve discrete logarithm: https://en.wikipedia.org/wiki/Baby-step_giant-step
