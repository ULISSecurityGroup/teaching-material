#!/usr/bin/env python3

from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad

# 50^6

def get_key():
    for a in range(30, 80):
        for b in range(30, 80):
            for c in range(30, 80):
                yield bytes([a, b, c])

# Step 1
# we encrypt plaintext in all possible c1
table = {}
for k in get_key():
    key1 = b"\x00" * 13 + k
    cipher1 = AES.new(key=key1, mode=AES.MODE_ECB)
    pt = pad(b"a" * 16, AES.block_size)
    table[cipher1.encrypt(pt)] = key1

# print(table)

# c2 = encrypt(encrypt(pt))
# step 2
c2 = bytes.fromhex("d5919b33e8b8e6776a84e289850ab1d09258f2742c3241be45de931d3bfbb6d8")
for k2 in get_key():
    key2 = k2 + b"\x00" * 13
    cipher2 = AES.new(key=key2, mode=AES.MODE_ECB)
    dec = cipher2.decrypt(c2)
    if dec in table:
        key1 = table[dec]
        print(key1.hex())
        print(key2.hex())
        flag = bytes.fromhex("b39a7b68c4dad9d434219e0a785118f3242acf2d8d06f5e700f5f6520449d64b486a5fdd3a1b3c2a99fbc9426fb0b276")
        cipher1 = AES.new(key=key1, mode=AES.MODE_ECB)
        print(cipher1.decrypt(cipher2.decrypt(flag)))
        exit(0)

