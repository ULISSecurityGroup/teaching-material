from ptrlib import *
from Crypto.Util.number import long_to_bytes

r = remote('127.0.0.1', 1337)

r.sendlineafter('> ', '1')
c = int(r.recvline()[4:])
n = int(r.recvline()[4:])
e = 65537

r.sendlineafter('> ', '2')
r.sendlineafter('c = ', str(c))
last = int(r.recvline()[4:])



def oracle(x):
    global last
    r.sendlineafter('> ', '2')
    r.sendlineafter('c = ', str(x))
    m = int(r.recvline()[4:])
    if m != (last * 2) % 2:
        ret = 1
    else:
        ret = 0
    last = m
    return ret


m = lsb_leak_attack(oracle, n, e, c)
print(long_to_bytes(m))
