#!/usr/bin/env python3
from pwn import *
from Crypto.Util.number import *
from fractions import Fraction
from math import ceil

r = remote('127.0.0.1', 1337)

r.sendlineafter('> ', '1')
c = int(r.recvline()[4:])
n = int(r.recvline()[4:])
e = 65537

r.sendlineafter('> ', '2')
r.sendlineafter('c = ', str(c))
last = int(r.recvline()[4:])



def oracle(x):
    global last
    r.sendlineafter('> ', '2')
    r.sendlineafter('c = ', str(x))
    m = int(r.recvline()[4:])
    if m != (last * 2) % 2:
        ret = 1
    else:
        ret = 0
    last = m
    return ret

# imported from ptrlib
def lsb_leak_attack(lsb_oracle, n, e, c):

    L = n.bit_length()
    t = L // 100
    left, right = 0, n
    c2 = c
    while right - left > 1:
        c2 = (c2 * pow(2, e, n)) % n
        oracle = lsb_oracle(c2)
        if oracle == 1:
            left = (left+right)/2
        elif oracle == 0:
            right = (left+right)/2
        else:
            raise ValueError("The function `lsb_oracle` must return 1 or 0")

    return int(ceil(left))

m = lsb_leak_attack(oracle, n, e, c)
print(long_to_bytes(m))
r.interactive()
