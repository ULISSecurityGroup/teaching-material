\documentclass[aspectratio=169]{beamer}

\usepackage{circuitikz}
\usepackage{fontspec}
\usepackage{listings}

\usetikzlibrary{positioning}

\setmainfont{DejaVu Sans}

\setbeamercolor{frametitle}{fg=orange}
\setbeamercolor{background canvas}{bg=black!90}
\setbeamercolor{normal text}{fg=white}
\setbeamertemplate{itemize item}{\color{orange}$\blacksquare$}
\setbeamertemplate{itemize subitem}{\color{orange}$\blacktriangleright$}
\setbeamertemplate{navigation symbols}{}

\title{\color{orange}Introduction to programmable hardware security}
\author{Davide Berardi <berardi.dav@gmail.com>}

\begin{document}
\setbeamertemplate{frametitle}[default][center]

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}
    \frametitle{Hardware vs software}
    \begin{center}
        \begin{quote}
            Hardware is the part of the computer you can kick.
        \end{quote}

        \Huge \textbf{Software $\neq$ code !}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Programmable hardware}
    We will focus on {\em programmable } hardware to
    create every digital function we want.

    \begin{itemize}
        \item {\em We can't program analog functions (unless your
            programmable hardware have Digital to Analog
            Converters (DACs)).}
        \item To do so we need some sort of reconfigurable hardware
            device.
    \end{itemize}

    \begin{minipage}{.49\textwidth}
        \begin{center}
            FPGA\\
            \textbf{F}ield \textbf{P}rogrammable
            \textbf{G}ate \textbf{A}rray\\
            \includegraphics[scale=.3]{img/ice40hx8k}
        \end{center}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
        \begin{center}
            CPLD\\
            \textbf{C}omplex \textbf{P}rogrammable
            \textbf{L}ogic \textbf{D}evice\\
            \includegraphics[scale=.3]{img/cpld}
        \end{center}
    \end{minipage}
    \begin{itemize}
        \item We will focus on \textbf{Lattice} boards, expecially on
    ice40hx8k, using the ice40hx8k-evb evaluation board from Olimex\footnote{\url{https://www.olimex.com/Products/FPGA/iCE40/iCE40HX8K-EVB/open-source-hardware}}.
        \item Other famous productors of FPGAs are Xilinx and Altera.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Programming FPGA}
    \begin{itemize}
        \item A FPGA and a CPU are entirely different beasts.

        \item While CPU implement basically a state machine, the FPGA could
            implement completely indipendent blocks.

        \item You can imagine it like an operating system, where you have indipendent
            process and you can combine them by IPC.\\
            \texttt{head -10 /etc/passwd | tail -1 | tr ':' ' '}
    \end{itemize}
    \begin{minipage}{.49\textwidth}
        CPU typical workflow\\
        \begin{center}
        \begin{tikzpicture}
            \node[draw,circle] (state1) {};
            \node[draw,circle,below=.5cm of state1] (state2) {};
            \node[draw,circle,below=.5cm of state2] (state3) {};
            \draw[-latex] (state1) -- (state2);
            \draw[-latex] (state2) -- (state3);
        \end{tikzpicture}
        \end{center}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
        FPGA typical workflow\\
        \begin{center}
        \begin{tikzpicture}
            \node[draw,circle] (state1) {};
            \node[draw,circle,right=.5cm of state1] (state2) {};
            \node[draw,circle,right=.5cm of state2] (state3) {};
            \node[draw,circle,below=.5cm of state1] (state4) {};
            \node[draw,circle,right=.5cm of state4] (state5) {};
            \node[draw,circle,right=.5cm of state5] (state6) {};
            \node[draw,circle,below=.5cm of state4] (state7) {};
            \node[draw,circle,right=.5cm of state7] (state8) {};
            \node[draw,circle,right=.5cm of state8] (state9) {};
        \end{tikzpicture}
        \end{center}
    \end{minipage}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Boolean Functions and truth tables}
    To program the FPGA we will need to create boolean functions.
    These functions are built with boolean logics, we will focus on
    the following constructs:

    \begin{minipage}{.49\textwidth}
    \begin{center}
    Not\\
    \begin{circuitikz}
        \node [scale=.5,color=white,not port] (not){};
        \draw (not.in) -- (not.in) node[left] {A};
        \draw (not.out) -- (not.out) node[right] {$\lnot A$};
    \end{circuitikz}
    \begin{tabular}{c||c}
        A & $\lnot A$\\
        0 & 1 \\
        1 & 0
    \end{tabular}
    \end{center}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
    \begin{center}
    And\\
    \begin{circuitikz}
        \node [scale=.5,color=white,and port] (and) {};
        \draw (and.in 1) -- (and.in 1) node[left] {A};
        \draw (and.in 2) -- (and.in 2) node[left] {B};
        \draw (and.out) -- (and.out) node[right] {$A \land B$};
    \end{circuitikz}
    \begin{tabular}{c|c||c}
        A & B & $A \land B$\\
        0 & 0 & 0  \\
        0 & 1 & 0  \\
        1 & 0 & 0  \\
        1 & 1 & 1  \\
    \end{tabular}
    \end{center}
    \end{minipage}

    \begin{minipage}{.49\textwidth}
    \begin{center}
    Or\\
    \begin{circuitikz}
        \node [scale=.5,color=white,or port] (or) {};
        \draw (or.in 1) -- (or.in 1) node[left] {A};
        \draw (or.in 2) -- (or.in 2) node[left] {B};
        \draw (or.out) -- (or.out) node[right] {$A \lor B$};
    \end{circuitikz}
    \begin{tabular}{c|c||c}
        A & B & $A \lor B$\\
        0 & 0 & 0  \\
        0 & 1 & 1  \\
        1 & 0 & 1  \\
        1 & 1 & 1  \\
    \end{tabular}
    \end{center}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
    \begin{center}
    Xor\\
    \begin{circuitikz}
        \node [scale=.5,color=white,xor port] (xor) {};
        \draw (xor.in 1) -- (xor.in 1) node[left] {A};
        \draw (xor.in 2) -- (xor.in 2) node[left] {B};
        \draw (xor.out) -- (xor.out) node[right] {$A \oplus B$};
    \end{circuitikz}
    \begin{tabular}{c|c||c}
        A & B & $A \oplus B$ \\
        0 & 0 & 0  \\
        0 & 1 & 1  \\
        1 & 0 & 1  \\
        1 & 1 & 0  \\
    \end{tabular}
    \end{center}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{FPGA Structure - LUT}
    \begin{itemize}
        \item The basic block of the FPGA is called \textbf{L}ook \textbf{U}p \textbf{T}able.
        \item This entity is a programmable truth table, you can express every boolean logic function
        \item in a truth table, therefore you can express every digital function using this technique.

        \item When the input is applied to this table, the value is returned, almost instantly.
    \end{itemize}

    \begin{minipage}{.49\textwidth}
    \[
        \lnot C \land ( A \lor B \lor D )
    \]
    \end{minipage}
    \begin{minipage}{.49\textwidth}
    \begin{circuitikz}
        \draw[color=white] (4.5,.5) node[white, and port] (myand) {};
        \draw[color=white] (2.5,1.5) node[white, or port] (myor2) {};
        \draw[color=white] (.5,3) node[or port] (myor1) {};
        \draw[color=white] (0,0) node[not port, white, fill=white] (mynot) {};

        \draw (myor2.in 2) -- (myor2.in 2) node[left] {D};
        \draw (mynot.in) -- (mynot.in)   node[left] {C};
        \draw (myor1.in 2) -- (myor1.in 2) node[left] {B};
        \draw (myor1.in 1) -- (myor1.in 1) node[left] {A};
        \draw (myor1.out) -- (myor2.in 1);
        \draw (myor2.out) -- (myand.in 1);
        \draw (mynot.out) -- (myand.in 2);
        \draw (myand.out) -- (myand.out)  node[right] {Out};
    \end{circuitikz}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{FPGA Structure - LUT}
    \begin{minipage}{.49\textwidth}
    \begin{tabular}{c|c|c|c||c}
        \textbf{A} & \textbf{B} & \textbf{C} & \textbf{D} & \textbf{Out} \\
        0 & 0 & 0 & 0 & 0 \\
        0 & 0 & 0 & 1 & 1 \\
        0 & 0 & 1 & 0 & 0 \\
        0 & 0 & 1 & 1 & 0 \\
        0 & 1 & 0 & 0 & 1 \\
        0 & 1 & 0 & 1 & 1 \\
        0 & 1 & 1 & 0 & 0 \\
        0 & 1 & 1 & 1 & 0 \\
        1 & 0 & 0 & 0 & 1 \\
        1 & 0 & 0 & 1 & 1 \\
        1 & 0 & 1 & 0 & 0 \\
        1 & 0 & 1 & 1 & 0 \\
        1 & 1 & 0 & 0 & 1 \\
        1 & 1 & 0 & 1 & 1 \\
        1 & 1 & 1 & 0 & 0 \\
        1 & 1 & 1 & 1 & 0
    \end{tabular}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
    \[
        \lnot C \land ( A \lor B \lor D )
    \]
    \begin{circuitikz} \draw[color=white]
        (4.5,.5) node[and port] (myand) {}
        (2.5,1.5) node[or port] (myor2) {}
        (.5,3) node[or port] (myor1) {}
        (0,0) node[not port] (mynot) {}

        (myor2.in 2) -- (myor2.in 2) node[left] {D}
        (mynot.in) -- (mynot.in)   node[left] {C}
        (myor1.in 2) -- (myor1.in 2) node[left] {B}
        (myor1.in 1) -- (myor1.in 1) node[left] {A}
        (myor1.out) -- (myor2.in 1)
        (myor2.out) -- (myand.in 1)
        (mynot.out) -- (myand.in 2)
        (myand.out) -- (myand.out)  node[right] {Out}
        ;
    \end{circuitikz}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Boolean Logic Function - Full Adder}
    To create CPUs, we need arithmetic functions.  One simple
    example of this kind of functions is the Full Adder circuit,
    which simply adds two bits.

    \begin{minipage}{.49\textwidth}
    \[
        S = A \oplus B \oplus C
    \]
    \[
        Carry = ( A \oplus B \land C ) \lor ( A \land B )
    \]

    \begin{tabular}{c|c|c||c|c}
        \textbf{A} & \textbf{B} & \textbf{C} & \textbf{Sum} & \textbf{Carry} \\
        0 & 0 & 0 & 0 & 0 \\
        0 & 0 & 1 & 1 & 0 \\
        0 & 1 & 0 & 1 & 0 \\
        0 & 1 & 1 & 0 & 1 \\
        1 & 0 & 0 & 1 & 0 \\
        1 & 0 & 1 & 0 & 1 \\
        1 & 1 & 0 & 0 & 1 \\
        1 & 1 & 1 & 1 & 1
    \end{tabular}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
    \begin{circuitikz} \draw[color=white]
        (0,0)       node[xor port] (myxor1) {}
        (3.5,-.50)  node[xor port] (myxor2) {}
        (3.5,-2.50) node[xor port] (myxor3) {}
        (0,-2.7)    node[and port] (myand1) {}
        (1.76,-2)   node[and port] (myand2) {}

        (myand2.out) -- (myxor3.in 1)
        (myand1.out) -- (myxor3.in 2)
        (myxor2.out) -- (myxor2.out) node[right] {S}
        (myxor3.out) -- (myxor3.out) node[right] {Carry}

        (myand2.in 2) -- (myand2.in 2) node[left] {C}
        (myxor2.in 2) -- (myxor2.in 2) node[left] {C}

        (myxor2.in 1) -- (myxor1.out)
        (myxor1.out)  -- (myand2.in 1)

        (myxor1.in 1) -- (myxor1.in 1) node[left] {A}
        (myxor1.in 2) -- (myxor1.in 2) node[left] {B}

        (myand1.in 1) -- (myand1.in 1) node[left] {A}
        (myand1.in 2) -- (myand1.in 2) node[left] {B}
        ;
    \end{circuitikz}
    \end{minipage}

\end{frame}

\begin{frame}
    \frametitle{FPGA Structure - Programmable interconnect}
    While programming LUTs give us the possibility of create
    functions, they are limited in space.  Tipically a LUT block
    has 4 / 6 input and one output, how can we create, for instance,
    full adders?

    \begin{minipage}{.49\textwidth}
    \[
        S = A \oplus B \oplus C
    \]
    \[
        Carry = ( A \oplus B \land C ) \lor ( A \land B )
    \]

    \begin{tabular}{c|c|c||c|c}
        \textbf{A} & \textbf{B} & \textbf{C} & \textbf{Sum} & \textbf{Carry} \\
        0 & 0 & 0 & 0 & 0 \\
        0 & 0 & 1 & 1 & 0 \\
        0 & 1 & 0 & 1 & 0 \\
        0 & 1 & 1 & 0 & 1 \\
        1 & 0 & 0 & 1 & 0 \\
        1 & 0 & 1 & 0 & 1 \\
        1 & 1 & 0 & 0 & 1 \\
        1 & 1 & 1 & 1 & 1
    \end{tabular}
    \end{minipage}
    \begin{minipage}{.49\textwidth}
    \begin{circuitikz} \draw[color=white]
        (0,0)       node[xor port] (myxor1) {}
        (3.5,-.50)  node[xor port] (myxor2) {}
        (3.5,-2.50) node[xor port] (myxor3) {}
        (0,-2.7)    node[and port] (myand1) {}
        (1.76,-2)   node[and port] (myand2) {}

        (myand2.out) -- (myxor3.in 1)
        (myand1.out) -- (myxor3.in 2)
        (myxor2.out) -- (myxor2.out) node[right] {S}
        (myxor3.out) -- (myxor3.out) node[right] {Carry}

        (myand2.in 2) -- (myand2.in 2) node[left] {C}
        (myxor2.in 2) -- (myxor2.in 2) node[left] {C}

        (myxor2.in 1) -- (myxor1.out)
        (myxor1.out)  -- (myand2.in 1)

        (myxor1.in 1) -- (myxor1.in 1) node[left] {A}
        (myxor1.in 2) -- (myxor1.in 2) node[left] {B}

        (myand1.in 1) -- (myand1.in 1) node[left] {A}
        (myand1.in 2) -- (myand1.in 2) node[left] {B}
        ;
    \end{circuitikz}
    \end{minipage}

\end{frame}

\begin{frame}
    \frametitle{FPGA Structure - Flip Flop}
    A Flip-Flop D is a circuit which acts as a single bit memory.
    That is, if we put a single-bit data in the \textbf{D}ata input,
    and trigger a rising edge on the \textbf{C}lock pin, we get this
    data in output (\textbf{Q} with direct logic and
    \textbf{$\overline{Q}$} with inverted logic) until the Data
    change and Clock triggers.

    \begin{center}
        \includegraphics[scale=.3]{img/flipflopD}\\
        \includegraphics[scale=.5]{img/flipflopd-lanes}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{FPGA Structure - Programmable interconnect}
    We can concatenate LUTs!  To do so we need a programmable
    interconnection matrix.

    LUTs also have Flip Flops embedded in them, therefore we
    can create { \em state machines }.

    \begin{figure}
        \includegraphics{img/ice40hx8k-lutdetail}
        \caption{ice40 LP/HX datasheet}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{FPGA Structure - ice40hx8k}
    FPGA have also other components like PLLs, block RAMs, and
    programmable input output (attached to the connection matrix).

    \begin{figure}
        \centering
        \includegraphics[scale=.30]{img/ice40hx8k-structure}
        \caption{ice40 LP/HX datasheet}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Program phases}
    While the act of programming an FPGA is quite a simple task
    (just put the configuration of the LUTs and the programmable
    interconnection in the device), the procedure to get a valuable
    bitstream is quite complex in various aspects.

    \begin{center}
    % Immagine ciclo sviluppo
    \begin{tikzpicture}
        \node[
        ] (spec) {Specifications};
        \node[
            below=.5cm of spec
        ] (hdl) {High level design using HDL};
        \node[
            below=.5cm of hdl
        ] (synt) {Syntesys};
        \node[
            below=.5cm of synt
        ] (pnr) {P\&R};
        \node[
            below=.5cm of pnr
        ] (flashing) {Flashing / implementation};

        \draw[-latex] (spec) -- (hdl);
        \draw[-latex] (hdl) -- (synt);
        \draw[-latex] (synt) -- (pnr);
        \draw[-latex] (pnr) -- (flashing);
    \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{HDL}
    To create ``programs'' we need a programming language.  For this
    kind of task we will see mainly two languages: VHDL and Verilog.

    This hardware description languages differs from normal programming
    language because they creates bitstream, you need to think in a
    ``block'' way, not in a sequential way.
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{HDL - basic block}
    An example block in VHDL is the following one

    \begin{lstlisting}[
        language=VHDL,
        basicstyle=\tiny,
    ]
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity led_blinker is
    port (
        i_clock     : in  std_logic;
        o_led_drive : out std_logic
         );
end led_blinker;
architecture rtl of led_blinker is
    -- Counter for led blinking
    -- this is specific for your input clock!
    constant c_CNT_1HZ    : natural := 1250000;
    signal r_CNT_1HZ    : natural range 0 to c_CNT_1HZ;
    signal r_TOGGLE_1HZ : std_logic := '0';
    begin
        p_1_HZ : process (i_clock) is
        begin
            if rising_edge(i_clock) then
                if r_CNT_1HZ = c_CNT_1HZ - 1 then
                    r_TOGGLE_1HZ <= not r_TOGGLE_1HZ;
                    r_CNT_1HZ <= 0;
                else
                    r_CNT_1HZ <= r_CNT_1HZ + 1;
                end if;
            end if;
        end process p_1_HZ;
        o_led_drive <= r_TOGGLE_1HZ;
end rtl;
    \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{HDL - Testbed}
    HDL languages also have test-related constructs, this are called testbeds.

    \begin{lstlisting}[
        language=VHDL,
        basicstyle=\tiny,
    ]
architecture behave of led_blinker_tb is
        constant c_CLOCK_PERIOD : time := 40 ns;
        signal r_CLOCK          : std_logic := '0';
        signal w_LED_DRIVE      : std_logic;
        begin
        UUT: led_blinker
            port map (
                i_clock => r_CLOCK,
                o_led_drive => w_LED_DRIVE
                 );

        -- Clock generator
        p_CLK_GEN : process is
        begin
            wait for c_CLOCK_PERIOD / 2;
            r_CLOCK <= not r_CLOCK;
        end process p_CLK_GEN;

        p_reporter : process is
        begin
            if rising_edge(w_LED_DRIVE) then
                report "Led On!";
            elsif falling_edge(w_LED_DRIVE) then
                report "Led Off!";
            end if;
            wait on w_LED_DRIVE;
        end process p_reporter;
end behave;
    \end{lstlisting}
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Hardware security}
    We want to check if there are hardware backdoors in the logic of the
    circuit.

\begin{lstlisting}[
    language=VHDL,
    basicstyle=\tiny ]
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity lfsr is
    port (
        i_clock : in  std_logic;
        i_bdr   : in  std_logic;
        o_state : out std_logic_vector(6 downto 0)
         );
end lfsr;
architecture rtl of lfsr is
    signal r_state : std_logic_vector(6 downto 0) := "1111011";
    begin
        o_state <= r_state(6 downto 0);
        p_cnt : process (i_clock) is
        begin
            r_state(6) <= r_state(0);
            r_state(5) <= r_state(6) xor r_state(0);
            r_state(4) <= r_state(5);
            r_state(3) <= r_state(4);
            r_state(2) <= r_state(3);
            r_state(1) <= r_state(2);
            r_state(0) <= r_state(1);
            -- Reset procedure
            if rising_edge(i_bdr) then
                r_state <= "0000000";
            end if;
        end process p_cnt;
end rtl;
\end{lstlisting}
    % esempio con bug inserito
\end{frame}

\begin{frame}[containsverbatim]
    \frametitle{Hardware security}
    We want to check if there are hardware backdoors in the logic of
    the circuit.

    % spiegazione esempio con bug inserito
\begin{lstlisting}
    if rising_edge(i_bdr) then
        r_state <= "0000000";
    end if;
\end{lstlisting}
    \includegraphics[width=\textwidth]{img/dilbert}
\end{frame}

\begin{frame}
    \frametitle{Hardware security - some examples without HDL}

    Some example of hardware bugs and hardware related talks that can be analyzed:
    \begin{itemize}
        \item f00f bug
            \url{https://en.wikipedia.org/wiki/Pentium_F00F_bug}.
        \item SandSifter, analyzing processor hidden instructions within the processor itself
            \url{https://www.youtube.com/watch?v=ajccZ7LdvoQ}.
        \item Spectre, Meltdown
            \url{https://meltdownattack.com/}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Hardware security - Are we f*-ed?}
    Despite basically using source code,
    the entire hardware scena seems to be closed source.

    Therefore are we tied to closed source platforms for
    ever?
    \begin{center}
        \centering
        \includegraphics[scale=.66]{img/marx}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Hardware security - opencores}
    Opencores is a site with open source implementations
    of common useful devices

    \begin{center}
        \includegraphics[scale=1.6]{img/opencores}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Hardware security - riscV}
    RiscV is an open source ISA specification.
    We will have open source CPUs!

    \begin{center}
        \includegraphics{img/riscv}
    \end{center}

    Unfortunately, most of the software stack to program FPGA is
    closed source (Xilinx Vivado, Intel Quartus, ...).  We will see
    a fully open source toolchain (Yosys + Arachne PnR + icestorm).
\end{frame}

\begin{frame}
    \frametitle{Open Source IDE for HDL}
    ICEstudio is a full IDE which use a completely open source
    tool-chain.

    \begin{center}
    \includegraphics[width=.6\textwidth]{img/icestudio}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Static analysis of HDL code}
    The HDL code can be analyzed by using some
    automated tools.

    \begin{enumerate}
        \item Put the code in a RTL visualizer;
        \item Compare the visualized code with the specification
    \end{enumerate}

    For verilog there is a cool visualizer online
    \url{http://digitaljs.tilk.eu/}

    \includegraphics[width=\textwidth]{img/fulladder}
\end{frame}

\begin{frame}
    \frametitle{Formal verification of HDL}
    HDL can be formally verified to be accordant to the
    specification.

    \begin{itemize}
        \item The tools that do formal specification are really
            expensive, and a formal specification of your
            system shall be created to verify the correct
            behaviour of your system.
        \item A libre implementation by Clifford Wolf is
            embedded in yosys.
            This implementation uses z3 and other solvers
            to verify the circuit behaviour.
    \end{itemize}
    \url{https://www.youtube.com/watch?v=VJsMLPGg4U4}
\end{frame}

\begin{frame}
    \frametitle{Debug of HDL code - tools}
    As we saw we can simulate the source code using a specific
    program and an hardware specification in our testbed.

    On Linux, for example, we can use open source tools like GHDL for
    VHDL and Icarus for Verilog.

    % Immagine simulazione verilog
    \begin{center}
        \includegraphics[scale=.5]{img/ghdl-sim}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Debug of HDL code - tools}
    Sometimes the signals of the FPGA should be analized, in this
    case, a valid open source tool is called GTKwave.

    \begin{figure}
    % Screenshot gtkwave runner
        \includegraphics[width=.75\textwidth]{img/gtkwave}
        \caption{Image from \url{https://wiki.tcl-lang.org/page/GTKWave}}
    \end{figure}
\end{frame}

%\begin{frame}
%    \frametitle{Demo}
%    \begin{center}
%        Let's see a demo of this tools!\\
%        % Stand back I'm going to use science!
%        \includegraphics{img/science}
%    \end{center}
%\end{frame}

\begin{frame}
    \frametitle{Acknowledgements}

    \begin{center}
    An huge thanks to Engineer Michele Brian.\\
    Which followed me in the creation of this material and answered
    to all my silly questions. :)
    \end{center}
\end{frame}

\end{document}
